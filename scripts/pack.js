const fs = require('fs');
const os = require('os');
const path = require('path');
const childProcess = require('child_process');
const {Command} = require("commander");

const fsp = fs.promises;

const argsParser = new Command()
	.argument('[platform]', 'Platform to build')
	.option('--package <path>')
	.option('--version <version>')
	.option('--format <format>', 'Format of the package (ex: deb for the linux platform)')
	.showHelpAfterError()
	.parse();

const platform = argsParser.processedArgs[0] || process.platform;
const options = argsParser.opts();

const baseDir = path.resolve(__dirname, '..');
const pkgDataFile = options.package ? path.resolve(options.package) : path.join(baseDir, 'package.json');
const pkgData = JSON.parse(fs.readFileSync(pkgDataFile, "utf-8"));

let version = options.version || ('v' + pkgData.version);
let pkgVersion = version;
let priority;

let tagVersion = null;
if (version[0] == 'v') {
	let [major, medium, minor] = version.substr(1).split('.');
	minor = minor.padStart(3, '0');
	tagVersion = [major, medium, minor].join('.');
	pkgVersion = [major, medium].join('.');
	priority = [major, medium].join('');
}
const distsDir = `${baseDir}/dist`;
const distName = `postscriptum-app-${tagVersion ? 'v' + tagVersion : version}-${platform}`;
const distDir = `${distsDir}/${distName}`;

const packSrcDir = `${__dirname}/../src`;

const COMMON_EXES = ['bin/postscriptum', 'bin/postscriptum-pdf2svg', 'bin/postscriptum-app', 'node/node'];
const EXES = {
	'linux': COMMON_EXES.concat(['node_modules/@postscriptum.app/postpdf/dist/linux/postscriptum-postpdf', 'chromium/chrome', 'chromium/chrome_crashpad_handler', 'chromium/chrome_sandbox']),
	'darwin': COMMON_EXES.concat(['node_modules/@postscriptum.app/postpdf/dist/darwin/postscriptum-postpdf', 'Chromium.app/Contents/MacOS/Chromium']),
};

(async () => {
	try {
		console.log(`Packaging postscriptum-app-${version}-${platform}`);
		if (platform == "win32") await createWinInstaller();
		else if (platform == "linux" && options.format == "deb") await createDeb();
		else await createTar();
	} catch (error) {
		console.error(error);
		process.exit(1);
	}
})();

async function createWinInstaller() {
	const makensis = require('makensis');
	try {
		await makensis.compile(`${packSrcDir}/postscriptum.nsi`, {verbose: 4, define: {VERSION: version, MEDIUM_VERSION: pkgVersion}});
	} catch (error) {
		console.error(error.stdout || error.stderr);
		process.exit(1);
	}
}

function createTar() {
	const tar = require('tar-fs');
	const zlib = require('zlib');
	return new Promise(async (resolve, reject) => {
		const outputPath = `${distsDir}/${distName}.tar.gz`;
		await fsp.rm(outputPath, {force: true});
		const output = fs.createWriteStream(outputPath);
		tar.pack(distDir, {
			map: function (header) {
				if (header.type == 'directory' || platform == 'darwin' || EXES[platform].includes(header.name)) header.mode = parseInt('755', 8);
				else header.mode = parseInt('644', 8);
				header.name = `${distName}/${header.name}`;
				header.uid = header.gid = 0;
				return header;
			},
		}).pipe(zlib.createGzip()).pipe(output)
			.on('error', reject)
			.on('finish', resolve);
	});
}

async function createDeb() {
	if (!tagVersion) {
		console.error("Debian package only available on tag version");
		return;
	}
	const outputPath = `${distsDir}/${distName}.deb`;
	await fsp.rm(outputPath, {force: true});
	const tmpDir = await fsp.mkdtemp(path.join(os.tmpdir(), 'postscriptum-deb-'));
	const sysPsDir = `/opt/postscriptum-${pkgVersion}`;
	try {
		await fsp.mkdir(`${tmpDir}/DEBIAN`);
		// Provides/Breaks/Replaces : cf https://www.debian.org/doc/debian-policy/ch-relationships.html#replacing-whole-packages-forcing-their-removal
		let control = `Package: postscriptum-${pkgVersion}-app
Version: ${tagVersion}
Architecture: amd64
Maintainer: david@rivron.fr
Description: Postscriptum application
Section: text
Priority: optional
Depends: libgtk-3-0, libnss3, libasound2, libxss1, libx11-xcb1, libdrm2, libgbm1, libxshmfence1
Provides: postscriptum-app (= ${tagVersion})
Recommends: fonts-noto
`;
		if (pkgVersion == "0.8") {
			control += `Breaks: postscriptum-app (<< 0.8.101)
Replaces: postscriptum-app (<< 0.8.101)
`
		}
		await fsp.writeFile(`${tmpDir}/DEBIAN/control`, control);

		const postinst = /* language=sh */ `#!/bin/sh
set -e
if [ "$1" = "configure" ] ; then
	update-alternatives --quiet --install /usr/bin/postscriptum postscriptum ${sysPsDir}/bin/postscriptum ${priority}
	update-alternatives --quiet --install /usr/bin/postscriptum-app postscriptum-app ${sysPsDir}/bin/postscriptum-app ${priority} \\
		--slave /usr/share/applications/postscriptum.desktop postscriptum.desktop ${sysPsDir}/postscriptum.desktop

	if [ -x /usr/bin/update-desktop-database ]; then update-desktop-database -q /usr/share/applications; fi
	# Write the apparmor profile if apparmor 4 exists
	aaVersion=$(apparmor_parser -V 2>/dev/null | grep -oP '(?<=version )\\d+(?=\\.)' | cat)
	if [ -n "$aaVersion" ] && [ "$aaVersion" -ge 4 ]; then
		cat << EOF > /etc/apparmor.d/postscriptum-${pkgVersion}-app
abi <abi/4.0>,

/opt/postscriptum-${pkgVersion}/chromium/chrome flags=(unconfined) {
  userns,
}
EOF
		if [ "$(aa-enabled 2>/dev/null)" = "Yes" ]; then
			# Enable the apparmor profile
			apparmor_parser -r -T -W /etc/apparmor.d/postscriptum-${pkgVersion}-app
		fi
	fi
fi`
		await fsp.writeFile(`${tmpDir}/DEBIAN/postinst`, postinst);

		const prerm = /* language=sh */ `#!/bin/sh
set -e
if [ "$1" = "remove" ] ; then
	update-alternatives --quiet --remove postscriptum ${sysPsDir}/bin/postscriptum
	update-alternatives --quiet --remove postscriptum-app ${sysPsDir}/bin/postscriptum-app
	
	if [ -x /usr/bin/update-desktop-database ]; then update-desktop-database -q /usr/share/applications; fi
	rm -f /etc/apparmor.d/postscriptum-${pkgVersion}-app
fi`
		await fsp.writeFile(`${tmpDir}/DEBIAN/prerm`, prerm);

		await fsp.cp(`${distsDir}/${distName}`, `${tmpDir}${sysPsDir}`, {recursive: true});
		await copyAndReplaceProps(`${packSrcDir}/postscriptum.desktop`, `${tmpDir}${sysPsDir}/postscriptum.desktop`, {VERSION: pkgVersion});
		await fsp.mkdir(`${tmpDir}${sysPsDir}/chromium/extensions`, {recursive: true});
		await chmod(tmpDir, "u=rwx,go=rx", 'd');
		await chmod(tmpDir, "u=rw,go=r", 'f');
		await chmod(`${tmpDir}/DEBIAN/postinst`, "u=rwx,go=rx", 'f');
		await chmod(`${tmpDir}/DEBIAN/prerm`, "u=rwx,go=rx", 'f');
		for (const exe of EXES[platform]) {
			await chmod(`${tmpDir}${sysPsDir}/${exe}`, "u=rwx,go=rx");
		}

		await exec(`dpkg-deb --root-owner-group -b "${tmpDir}" "${distsDir}/${distName}.deb"`);
	} finally {
		await fsp.rm(tmpDir, {recursive: true, force: true});
	}
}

async function exec(command) {
	return new Promise((resolve, reject) => {
		childProcess.exec(command, (error, stdout, stderr) => {
			if (error) reject(error);
			else if (stderr) reject(stderr);
			else resolve();
		});
	});
}

async function chmod(path, mode, type) {
	const typeArg = type ? `-type ${type}` : '';
	return new Promise((resolve, reject) => {
		childProcess.exec(`find "${path}" ${typeArg} -exec chmod ${mode} {} \\;`, (error, stdout, stderr) => {
			if (error) reject(error);
			else if (stderr) reject(stderr);
			else resolve();
		});
	});
}

async function copyAndReplaceProps(srcPath, destPath, {VERSION}) {
	let src = await fsp.readFile(srcPath, 'utf-8');
	await fsp.mkdir(path.dirname(destPath), {recursive: true});
	await fsp.writeFile(destPath, src.replaceAll('${VERSION}', VERSION));
}
