!include MUI2.nsh
!include "FileFunc.nsh"

Name "Postscriptum"

!define SRC_DIR "..\dist\postscriptum-app-${VERSION}-win32"
!define ICON_PATH "node_modules\@postscriptum.app\ext\dist\icons\ps.ico"

OutFile ..\dist\postscriptum-app-${VERSION}-win32.exe
InstallDir "$PROGRAMFILES64\Postscriptum"

RequestExecutionLevel admin

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "../LICENSE"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
!insertmacro MUI_LANGUAGE "English"

Section
	SetOutPath $INSTDIR
	File /r "${SRC_DIR}\"
	nsExec::Exec '"$INSTDIR\node\node.exe" "$INSTDIR\node_modules\@postscriptum.app\cli\dist\messages.js" --register'
   CreateShortCut "$SMPROGRAMS\Postscriptum.lnk" "$INSTDIR\bin\postscriptum-app.bat" "" \
   	"$INSTDIR\${ICON_PATH}" "" SW_SHOWMINIMIZED

	WriteUninstaller "$INSTDIR\Uninstall.exe"
	!define UNINSTALL_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\Postscriptum"
	WriteRegStr HKLM ${UNINSTALL_KEY} "DisplayName" "Postscriptum"
	WriteRegStr HKLM ${UNINSTALL_KEY} "DisplayIcon" "$\"$INSTDIR\${ICON_PATH}$\""
	WriteRegStr HKLM ${UNINSTALL_KEY} "DisplayVersion" "${VERSION}"
	WriteRegStr HKLM ${UNINSTALL_KEY} "Publisher" "David Rivron"
   WriteRegStr HKLM ${UNINSTALL_KEY} "UninstallString" "$\"$INSTDIR\Uninstall.exe$\""
	WriteRegStr HKLM ${UNINSTALL_KEY} "QuietUninstallString" "$\"$INSTDIR\Uninstall.exe$\" /S"
	WriteRegDWORD HKLM ${UNINSTALL_KEY} "NoModify" 1
	WriteRegDWORD HKLM ${UNINSTALL_KEY} "NoRepair" 1

	${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
	IntFmt $0 "0x%08X" $0
	WriteRegDWORD HKLM ${UNINSTALL_KEY} "EstimatedSize" "$0"
SectionEnd

Section "Uninstall"
	Delete "$SMPROGRAMS\Postscriptum.lnk"
	RMDir /R "$INSTDIR\bin"
	RMDir /R "$INSTDIR\node_modules"
	RMDir /R "$INSTDIR\chromium"
	RMDir /R "$INSTDIR\node"
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Postscriptum"
	Delete "$INSTDIR\Uninstall.exe"
	RMDir "$INSTDIR"
SectionEnd
